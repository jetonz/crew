﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;

namespace Crew.Data.Entities
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser<Guid>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
