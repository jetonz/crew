﻿using System;
using System.Collections.Generic;

namespace Crew.Data.Models
{
    public partial class TeamMemberRoles
    {
        public Guid RoleId { get; set; }
        public Guid TeamMemberId { get; set; }

        public virtual Roles Role { get; set; }
        public virtual TeamMembers TeamMember { get; set; }
    }
}
