﻿using System;
using System.Collections.Generic;

namespace Crew.Data.Models
{
    public partial class Accounts
    {
        public Accounts()
        {
            Projects = new HashSet<Projects>();
        }

        public Guid Id { get; set; }
        public string Skype { get; set; }
        public string Phone { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public short Status { get; set; }
        public string Description { get; set; }
        public string ContactPersons { get; set; }
        public string Email { get; set; }
        public Guid CompanyId { get; set; }
        public byte[] Logo { get; set; }

        public virtual ICollection<Projects> Projects { get; set; }
        public virtual Companies Company { get; set; }
    }
}
