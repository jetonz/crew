﻿using System;
using System.Collections.Generic;

namespace Crew.Data.Models
{
    public partial class ReportingPeriods
    {
        public Guid Id { get; set; }
        public Guid ProjectId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsActive { get; set; }

        public virtual Projects Project { get; set; }
    }
}
