﻿using System;
using System.Collections.Generic;

namespace Crew.Data.Models
{
    public partial class TeamMembers
    {
        public TeamMembers()
        {
            DailyActivities = new HashSet<DailyActivities>();
            TeamMemberRoles = new HashSet<TeamMemberRoles>();
        }

        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid ProjectId { get; set; }
        public string Specialization { get; set; }
        public int? Rate { get; set; }
        public short Level { get; set; }
        public bool IsRemoved { get; set; }

        public virtual ICollection<DailyActivities> DailyActivities { get; set; }
        public virtual ICollection<TeamMemberRoles> TeamMemberRoles { get; set; }
        public virtual Projects Project { get; set; }
        public virtual Users User { get; set; }
    }
}
