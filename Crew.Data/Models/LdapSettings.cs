﻿using System;
using System.Collections.Generic;

namespace Crew.Data.Models
{
    public partial class LdapSettings
    {
        public Guid Id { get; set; }
        public Guid CompanyId { get; set; }
        public string Domain { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string BaseDn { get; set; }

        public virtual Companies Company { get; set; }
    }
}
