﻿using System;
using System.Collections.Generic;

namespace Crew.Data.Models
{
    public partial class Companies
    {
        public Companies()
        {
            Accounts = new HashSet<Accounts>();
            LdapSettings = new HashSet<LdapSettings>();
            Users = new HashSet<Users>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Info { get; set; }
        public string Logo { get; set; }
        public bool IsActive { get; set; }

        public virtual ICollection<Accounts> Accounts { get; set; }
        public virtual ICollection<LdapSettings> LdapSettings { get; set; }
        public virtual ICollection<Users> Users { get; set; }
    }
}
