﻿using System;
using System.Collections.Generic;

namespace Crew.Data.Models
{
    public partial class ProjectCategories
    {
        public ProjectCategories()
        {
            DailyActivities = new HashSet<DailyActivities>();
        }

        public Guid Id { get; set; }
        public bool IsBillable { get; set; }
        public bool IsReportingEnabled { get; set; }
        public string Name { get; set; }
        public Guid? ParentId { get; set; }
        public Guid ProjectId { get; set; }
        public bool IsRemoved { get; set; }

        public virtual ICollection<DailyActivities> DailyActivities { get; set; }
        public virtual ProjectCategories Parent { get; set; }
        public virtual ICollection<ProjectCategories> InverseParent { get; set; }
        public virtual Projects Project { get; set; }
    }
}
