﻿using System;
using System.Collections.Generic;

namespace Crew.Data.Models
{
    public partial class UserRoles
    {
        public Guid RoleId { get; set; }
        public Guid UserId { get; set; }

        public virtual Roles Role { get; set; }
        public virtual Users User { get; set; }
    }
}
