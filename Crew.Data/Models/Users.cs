﻿using System;
using System.Collections.Generic;

namespace Crew.Data.Models
{
    public partial class Users
    {
        public Users()
        {
            TeamMembers = new HashSet<TeamMembers>();
            UserRoles = new HashSet<UserRoles>();
        }

        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public DateTime? Created { get; set; }
        public Guid CompanyId { get; set; }
        public byte[] Avatar { get; set; }
        public string SecurityStamp { get; set; }
        public bool HasRight { get; set; }

        public virtual ICollection<TeamMembers> TeamMembers { get; set; }
        public virtual ICollection<UserRoles> UserRoles { get; set; }
        public virtual Companies Company { get; set; }
    }
}
