﻿using System;
using System.Collections.Generic;

namespace Crew.Data.Models
{
    public partial class Projects
    {
        public Projects()
        {
            ProjectCategories = new HashSet<ProjectCategories>();
            ReportingPeriods = new HashSet<ReportingPeriods>();
            TeamMembers = new HashSet<TeamMembers>();
        }

        public Guid Id { get; set; }
        public Guid AccountId { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public bool IsActive { get; set; }
        public bool IsSuspended { get; set; }

        public virtual ICollection<ProjectCategories> ProjectCategories { get; set; }
        public virtual ICollection<ReportingPeriods> ReportingPeriods { get; set; }
        public virtual ICollection<TeamMembers> TeamMembers { get; set; }
        public virtual Accounts Account { get; set; }
    }
}
