﻿using System;
using System.Collections.Generic;

namespace Crew.Data.Models
{
    public partial class DailyActivities
    {
        public Guid Id { get; set; }
        public short Minutes { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }
        public Guid TeamMemberId { get; set; }
        public Guid ProjectCategoryId { get; set; }
        public bool IsBillable { get; set; }

        public virtual ProjectCategories ProjectCategory { get; set; }
        public virtual TeamMembers TeamMember { get; set; }
    }
}
