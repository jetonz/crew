﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Crew.Data.Models
{
    public partial class CrewDbContext : DbContext
    {
        public virtual DbSet<Accounts> Accounts { get; set; }
        public virtual DbSet<Companies> Companies { get; set; }
        public virtual DbSet<DailyActivities> DailyActivities { get; set; }
        public virtual DbSet<LdapSettings> LdapSettings { get; set; }
        public virtual DbSet<MigrationHistory> MigrationHistory { get; set; }
        public virtual DbSet<ProjectCategories> ProjectCategories { get; set; }
        public virtual DbSet<Projects> Projects { get; set; }
        public virtual DbSet<ReportingPeriods> ReportingPeriods { get; set; }
        public virtual DbSet<Roles> Roles { get; set; }
        public virtual DbSet<TeamMemberRoles> TeamMemberRoles { get; set; }
        public virtual DbSet<TeamMembers> TeamMembers { get; set; }
        public virtual DbSet<UserRoles> UserRoles { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            #warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
         //   optionsBuilder.UseSqlServer(@"data source=185.48.4.142;initial catalog=CrewDb;persist security info=True;user id=miPoint;password=q1w2e3R4t5y6;MultipleActiveResultSets=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Accounts>(entity =>
            {
                entity.HasIndex(e => e.CompanyId)
                    .HasName("IX_CompanyId");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.CompanyId).HasDefaultValueSql("'00000000-0000-0000-0000-000000000000'");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(60)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Phone).HasMaxLength(50);

                entity.Property(e => e.Skype).HasMaxLength(50);

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.Accounts)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_dbo.Accounts_dbo.Companies_CompanyId");
            });

            modelBuilder.Entity<Companies>(entity =>
            {
                entity.HasIndex(e => e.Name)
                    .HasName("IX_Name")
                    .IsUnique();

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Address).HasMaxLength(50);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.IsActive).HasDefaultValueSql("1");

                entity.Property(e => e.Logo).HasMaxLength(100);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Phone).HasMaxLength(50);
            });

            modelBuilder.Entity<DailyActivities>(entity =>
            {
                entity.HasIndex(e => e.ProjectCategoryId)
                    .HasName("IX_ProjectCategoryId");

                entity.HasIndex(e => e.TeamMemberId)
                    .HasName("IX_TeamMemberId");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).IsRequired();

                entity.Property(e => e.IsBillable).HasDefaultValueSql("0");

                entity.HasOne(d => d.ProjectCategory)
                    .WithMany(p => p.DailyActivities)
                    .HasForeignKey(d => d.ProjectCategoryId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_dbo.DailyActivities_dbo.ProjectCategories_ProjectCategoryId");

                entity.HasOne(d => d.TeamMember)
                    .WithMany(p => p.DailyActivities)
                    .HasForeignKey(d => d.TeamMemberId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_dbo.DailyActivities_dbo.TeamMembers_TeamMemberId");
            });

            modelBuilder.Entity<LdapSettings>(entity =>
            {
                entity.HasIndex(e => e.CompanyId)
                    .HasName("IX_CompanyId");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.BaseDn)
                    .IsRequired()
                    .HasColumnName("BaseDN")
                    .HasMaxLength(100);

                entity.Property(e => e.Domain)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.LdapSettings)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_dbo.LdapSettings_dbo.Companies_CompanyId");
            });

            modelBuilder.Entity<MigrationHistory>(entity =>
            {
                entity.HasKey(e => new { e.MigrationId, e.ContextKey })
                    .HasName("PK_dbo.__MigrationHistory");

                entity.ToTable("__MigrationHistory");

                entity.Property(e => e.MigrationId).HasMaxLength(150);

                entity.Property(e => e.ContextKey).HasMaxLength(300);

                entity.Property(e => e.Model).IsRequired();

                entity.Property(e => e.ProductVersion)
                    .IsRequired()
                    .HasMaxLength(32);
            });

            modelBuilder.Entity<ProjectCategories>(entity =>
            {
                entity.HasIndex(e => e.ParentId)
                    .HasName("IX_ParentId");

                entity.HasIndex(e => e.ProjectId)
                    .HasName("IX_ProjectId");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.IsRemoved).HasDefaultValueSql("0");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.InverseParent)
                    .HasForeignKey(d => d.ParentId)
                    .HasConstraintName("FK_dbo.ProjectCategories_dbo.ProjectCategories_ParentId");

                entity.HasOne(d => d.Project)
                    .WithMany(p => p.ProjectCategories)
                    .HasForeignKey(d => d.ProjectId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_dbo.ProjectCategories_dbo.Projects_ProjectId");
            });

            modelBuilder.Entity<Projects>(entity =>
            {
                entity.HasIndex(e => e.AccountId)
                    .HasName("IX_AccountId");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.IsSuspended).HasDefaultValueSql("0");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.Projects)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_dbo.Projects_dbo.Accounts_AccountId");
            });

            modelBuilder.Entity<ReportingPeriods>(entity =>
            {
                entity.HasIndex(e => e.ProjectId)
                    .HasName("IX_ProjectId");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.HasOne(d => d.Project)
                    .WithMany(p => p.ReportingPeriods)
                    .HasForeignKey(d => d.ProjectId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_dbo.ReportingPeriods_dbo.Projects_ProjectId");
            });

            modelBuilder.Entity<Roles>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TeamMemberRoles>(entity =>
            {
                entity.HasKey(e => new { e.RoleId, e.TeamMemberId })
                    .HasName("PK_dbo.TeamMemberRoles");

                entity.HasIndex(e => e.RoleId)
                    .HasName("IX_RoleId");

                entity.HasIndex(e => e.TeamMemberId)
                    .HasName("IX_TeamMemberId");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.TeamMemberRoles)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK_dbo.TeamMemberRoles_dbo.Roles_RoleId");

                entity.HasOne(d => d.TeamMember)
                    .WithMany(p => p.TeamMemberRoles)
                    .HasForeignKey(d => d.TeamMemberId)
                    .HasConstraintName("FK_dbo.TeamMemberRoles_dbo.TeamMembers_TeamMemberId");
            });

            modelBuilder.Entity<TeamMembers>(entity =>
            {
                entity.HasIndex(e => new { e.UserId, e.ProjectId })
                    .HasName("IX_UniqTeamMember")
                    .IsUnique();

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.IsRemoved).HasDefaultValueSql("0");

                entity.Property(e => e.Specialization).HasMaxLength(128);

                entity.HasOne(d => d.Project)
                    .WithMany(p => p.TeamMembers)
                    .HasForeignKey(d => d.ProjectId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_dbo.TeamMembers_dbo.Projects_ProjectId");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TeamMembers)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_dbo.TeamMembers_dbo.Users_UserId");
            });

            modelBuilder.Entity<UserRoles>(entity =>
            {
                entity.HasKey(e => new { e.RoleId, e.UserId })
                    .HasName("PK_dbo.UserRoles");

                entity.HasIndex(e => e.RoleId)
                    .HasName("IX_RoleId");

                entity.HasIndex(e => e.UserId)
                    .HasName("IX_UserId");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.UserRoles)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK_dbo.UserRoles_dbo.Roles_RoleId");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserRoles)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_dbo.UserRoles_dbo.Users_UserId");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasIndex(e => e.CompanyId)
                    .HasName("IX_CompanyId");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.HasRight).HasDefaultValueSql("0");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Login)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_dbo.Users_dbo.Companies_CompanyId");
            });
        }
    }
}