﻿using System;
using System.Collections.Generic;

namespace Crew.Data.Models
{
    public partial class Roles
    {
        public Roles()
        {
            TeamMemberRoles = new HashSet<TeamMemberRoles>();
            UserRoles = new HashSet<UserRoles>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public short RoleType { get; set; }

        public virtual ICollection<TeamMemberRoles> TeamMemberRoles { get; set; }
        public virtual ICollection<UserRoles> UserRoles { get; set; }
    }
}
