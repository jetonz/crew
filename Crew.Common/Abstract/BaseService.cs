using System;
using System.Globalization;
using Crew.Common.Interfaces.Repositories;
using Crew.Common.Interfaces.Services;

namespace Crew.Common.Abstract
{
    public abstract class BaseService : IBaseService
    {
        protected BaseService(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }

        public IUnitOfWork UnitOfWork { get; }
    }
}