﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Crew.Common.Interfaces.Repositories;
using Crew.Data;

namespace Crew.Common.Abstract
{
    public abstract class BaseRepository<T> : Disposable, IRepository<T> where T : class
    {
        protected BaseRepository(ApplicationDbContext context)
        {
            DataContext = context;
        }

        protected ApplicationDbContext DataContext { get; }

        public virtual async Task<T> GetAsync(object id)
        {
            if (!(id is Guid))
                id = new Guid(id.ToString());
            var result = DataContext.Set<T>().Find(id);
            return await Task.FromResult(result);
        }

        public virtual async Task AttachAsync(T entity)
        {
            await Task.Run(() => { DataContext.Set<T>().Attach(entity); });
        }

        public virtual async Task<IQueryable<T>> GetAllAsync()
        {
            return await Task.FromResult(DataContext.Set<T>());
        }

        public virtual async Task InsertAsync(T entity)
        {
            await Task.Run(() => { DataContext.Set<T>().Add(entity); });
        }

        public virtual async Task DeleteAsync(T entity)
        {
            await Task.Run(() => { DataContext.Set<T>().Remove(entity); });
        }

        public virtual async Task DeleteRangeAsync(IQueryable<T> entities)
        {
            await Task.Run(() => { DataContext.Set<T>().RemoveRange(entities); });
        }

        public virtual async Task SubmitChangesAsync()
        {
            await DataContext.SaveChangesAsync().ConfigureAwait(true);
        }

        public bool IsDisposed { get; private set; }

        protected override void Dispose(bool disposing)
        {
            if (IsDisposed)
                return;

            if (disposing)
                DataContext?.Dispose();

            IsDisposed = true;
            base.Dispose(disposing);
        }
    }
}