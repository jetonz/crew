﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace Crew.Common.Interfaces.Repositories
{
    public interface IRepository : IDisposable
    {
        bool IsDisposed { get; }
        Task SubmitChangesAsync();
    }

    public interface IRepository<T> : IRepository where T : class
    {
        Task<T> GetAsync(object id);
        Task AttachAsync(T entity);
        Task<IQueryable<T>> GetAllAsync();
        Task InsertAsync(T entity);
        Task DeleteAsync(T entity);
        Task DeleteRangeAsync(IQueryable<T> entities);
    }
}