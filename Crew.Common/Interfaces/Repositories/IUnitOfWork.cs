﻿using System;
using System.Threading.Tasks;

namespace Crew.Common.Interfaces.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        Task SubmitChangesAsync();
        Task ExecuteSqlCommand(string command, object[] parameters);
    }
}