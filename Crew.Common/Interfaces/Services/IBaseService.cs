﻿using Crew.Common.Interfaces.Repositories;

namespace Crew.Common.Interfaces.Services
{
    public interface IBaseService
    {
        IUnitOfWork UnitOfWork { get; }
    }
}