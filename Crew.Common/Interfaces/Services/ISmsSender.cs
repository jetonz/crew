﻿using System.Threading.Tasks;

namespace Crew.Common.Interfaces.Services
{
    public interface ISmsSender
    {
        Task SendSmsAsync(string number, string message);
    }
}
