﻿using System.Threading;
using System.Threading.Tasks;
using Crew.Common.Abstract;
using Crew.Common.Interfaces.Repositories;
using Crew.Data;
using Microsoft.EntityFrameworkCore;

namespace Crew.Repositories
{
    public class UnitOfWork : Disposable, IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
        }

        public bool IsDisposed { get; private set; }

        //public IPatientInEventsRepository PatientInEventsRepository => _patientInEventsRepository
        //                                                   ??
        //                                                   (_patientInEventsRepository =
        //                                                       new PatientInEventsRepository(_context));
        public async Task SubmitChangesAsync()
        {
            await _context.SaveChangesAsync().ConfigureAwait(true);
        }

        public async Task ExecuteSqlCommand(string command, object[] parameters)
        {
            await _context.Database.ExecuteSqlCommandAsync(command,
                CancellationToken.None, parameters);
        }

        private void DisposeInstances()
        {
            //_patientInEventsRepository?.Dispose();
        }

        protected override void Dispose(bool disposing)
        {
            if (IsDisposed)
                return;

            if (disposing)
                DisposeInstances();

            IsDisposed = true;
            base.Dispose(disposing);
        }
    }
}