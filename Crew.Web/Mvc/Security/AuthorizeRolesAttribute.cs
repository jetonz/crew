﻿using Crew.Web.Models.Enums;
using Microsoft.AspNetCore.Authorization;

namespace Crew.Web.Mvc.Security
{
    public class AuthorizeRolesAttribute : AuthorizeAttribute
    {
        public AuthorizeRolesAttribute(params RolesEnum[] roles)
        {
            Roles = string.Join(",", roles);
        }
    }
}