﻿using System;
using System.Security.Claims;

namespace Crew.Web.Mvc.Security
{
    public class ApplicationClaimsPrincipal : ClaimsPrincipal
    {
        public ApplicationClaimsPrincipal(ClaimsPrincipal principal)
            : base(principal)
        {
        }

        public Guid Id
        {
            get
            {
                var value = FindFirst("sub").Value;
                return new Guid(value);
            }
        }
    }
}