﻿using Crew.Web.Mvc.Security;
using Microsoft.AspNetCore.Mvc;

namespace Crew.Web.Mvc.Common
{
    public class BaseController : Controller
    {
        private ApplicationClaimsPrincipal _principal;

        public new virtual ApplicationClaimsPrincipal User => _principal ??
                                                              (_principal = new ApplicationClaimsPrincipal(base.User));
    }
}