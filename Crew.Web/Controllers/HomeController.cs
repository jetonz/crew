﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Crew.Web.Mvc.Common;
using Microsoft.AspNetCore.Mvc;

namespace Crew.Web.Controllers
{
    public class HomeController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
