﻿using System.ComponentModel.DataAnnotations;

namespace Crew.Web.Models.ViewModels.Account
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
