﻿using System.ComponentModel.DataAnnotations;

namespace Crew.Web.Models.ViewModels.Account
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
