﻿namespace Crew.Web.Models.Enums
{
    public enum RolesEnum
    {
        Administrator,
        Consultant,
        Patient
    }
}