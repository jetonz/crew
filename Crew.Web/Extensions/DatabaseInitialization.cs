﻿using System;
using System.Threading.Tasks;
using Crew.Data;
using Crew.Web.Models.Enums;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;

namespace Crew.Web.Extensions
{
    public static class DatabaseInitialization
    {
        public static async Task InitDatabaseAsync(this IApplicationBuilder appBuilder)
        {
            await InitializeAsync(appBuilder);
            await InitializeRolesAsync(appBuilder);
        }

        private static async Task InitializeAsync(IApplicationBuilder appBuilder)
        {
            using (var scope = appBuilder.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
                await context.Database.MigrateAsync();
            }
        }

        private static async Task InitializeRolesAsync(IApplicationBuilder appBuilder)
        {
            using (var scope = appBuilder.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole<Guid>>>();
                foreach (var role in Enum.GetValues(typeof(RolesEnum)))
                {
                    if (await roleManager.RoleExistsAsync(role.ToString())) continue;
                    var newRole = new IdentityRole<Guid>(role.ToString());
                    await roleManager.CreateAsync(newRole);
                }
            }
        }
    }
}