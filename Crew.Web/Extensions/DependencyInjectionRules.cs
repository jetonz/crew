﻿using Crew.Common.Interfaces.Repositories;
using Crew.Common.Interfaces.Services;
using Crew.Repositories;
using Crew.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Crew.Web.Extensions
{
    public static class DependencyInjectionRules
    {
        public static void AddDependencyInjectionRules(this IServiceCollection services, IConfigurationRoot configuration)
        {
            services.AddSingleton(configuration);
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();
        }
    }
}