﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;

namespace Crew.Web.Extensions
{
    public static class AutoMappersRules
    {
        public static void AddAutoMapperRules(this IServiceCollection services)
        {
            services.AddAutoMapper(configuration =>
            {

            });
        }
    }
}